module CurrencyLayer
  module QuotationsFinderByDate
    URL = 'http://apilayer.net/api/historical'.freeze
    CURRENCIES = 'BRL,EUR,ARS'.freeze

    def call(date)
      begin
        response = RestClient::Request.execute(
          method: :get,
          url: URL,
          headers: {
            params: {
              access_key: ENV['CURRENCY_LAYER_ACCESS_KEY'],
              currencies: CURRENCIES,
              date: date
            }
          }
        )
        JSON.parse(response.body)
      rescue RestClient::ExceptionWithResponse => e
        e.response
      end
    end

    module_function :call
  end
end
