# InnventQuotation

This project is a [Rails](http://rubyonrails.org/) application that compare last seven days USD quotation with BLR, EUR and ARS currencies, through a dashboard.

## Dependencies

To run this project you must have:

* Valid [Currency Layer](https://currencylayer.com) access key.
* NodeJS (you should use https://github.com/creationix/nvm)
* Ruby 2.3.1 - You can use [rbenv](https://github.com/rbenv/rbenv)
* [PostgreSQL](http://www.postgresql.org/)
  * OSX - [Postgress.app](http://postgresapp.com/)
  * Linux - `$ sudo apt-get install postgresql`
  * Windows - [PostgreSQL for Windows](http://www.postgresql.org/download/windows/)


## Setup the project

1. Install the dependencies above
2. `$ git@bitbucket.org:hudsonsferreira/innvent-real-quotation.git` - Clone the project
3. `$ cd innvent-real-quotation` - Go into the project folder
4.  Set CURRENCY_LAYER_ACCESS_KEY value at `.env` file
5. `$ bin/setup` - Run the setup script
6. `$ bundle exec rspec specs` - Run the specs to see if everything is working fine

If everything goes OK, you can now run the project!

## Running the project

1. `$ bundle exec rails s` - Start the application server
2. Open [http://localhost:3000](http://localhost:3000)


## Quick deploy to Heroku (if you have access)

1. You are updated on `master` branch
2. `$ bundle exec rspec spec` - Run the specs to see if everything is working fine
3. `$ git push heroku master`
4. `$ heroku open` - To open your application
