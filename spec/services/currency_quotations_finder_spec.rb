require 'rails_helper'

RSpec.describe CurrencyQuotationsFinder do
  describe '.call' do
    let(:expected_response) do
      [{"success"=>true,
        "terms"=>"https://currencylayer.com/terms",
        "privacy"=>"https://currencylayer.com/privacy",
        "historical"=>true,
        "date"=>"2016-11-23",
        "timestamp"=>1479945599,
        "source"=>"USD",
        "quotes"=>{"USDBRL"=>3.4321, "USDEUR"=>0.948302, "USDARS"=>15.489581}},
       {"success"=>true,
        "terms"=>"https://currencylayer.com/terms",
        "privacy"=>"https://currencylayer.com/privacy",
        "historical"=>true,
        "date"=>"2016-11-24",
        "timestamp"=>1480031999,
        "source"=>"USD",
        "quotes"=>{"USDBRL"=>3.391699, "USDEUR"=>0.946797, "USDARS"=>15.514971}},
       {"success"=>true,
        "terms"=>"https://currencylayer.com/terms",
        "privacy"=>"https://currencylayer.com/privacy",
        "historical"=>true,
        "date"=>"2016-11-25",
        "timestamp"=>1480118399,
        "source"=>"USD",
        "quotes"=>{"USDBRL"=>3.422041, "USDEUR"=>0.943804, "USDARS"=>15.538041}},
       {"success"=>true,
        "terms"=>"https://currencylayer.com/terms",
        "privacy"=>"https://currencylayer.com/privacy",
        "historical"=>true,
        "date"=>"2016-11-26",
        "timestamp"=>1480204799,
        "source"=>"USD",
        "quotes"=>{"USDBRL"=>3.420804, "USDEUR"=>0.943804, "USDARS"=>15.540402}},
       {"success"=>true,
        "terms"=>"https://currencylayer.com/terms",
        "privacy"=>"https://currencylayer.com/privacy",
        "historical"=>true,
        "date"=>"2016-11-27",
        "timestamp"=>1480291199,
        "source"=>"USD",
        "quotes"=>{"USDBRL"=>3.421498, "USDEUR"=>0.942302, "USDARS"=>15.537979}},
       {"success"=>true,
        "terms"=>"https://currencylayer.com/terms",
        "privacy"=>"https://currencylayer.com/privacy",
        "historical"=>true,
        "date"=>"2016-11-28",
        "timestamp"=>1480377599,
        "source"=>"USD",
        "quotes"=>{"USDBRL"=>3.408203, "USDEUR"=>0.941395, "USDARS"=>15.53973}},
       {"success"=>true,
        "terms"=>"https://currencylayer.com/terms",
        "privacy"=>"https://currencylayer.com/privacy",
        "historical"=>true,
        "date"=>"2016-11-29",
        "timestamp"=>1480456993,
        "source"=>"USD",
        "quotes"=>{"USDBRL"=>3.401399, "USDEUR"=>0.938501, "USDARS"=>15.539945}}]  
    end

    it 'calls CurrencyLayer::QuotationsFinderByDate seven times' do
      expect(CurrencyLayer::QuotationsFinderByDate).to receive(:call).exactly(7).times

      described_class.call
    end

    it 'returns all currency quotations', :vcr do
      expect(described_class.call).to eq expected_response
    end
  end
end
