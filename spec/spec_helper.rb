# frozen_string_literal: true
if ENV['coverage'] == 'on'
  require 'simplecov'

  SimpleCov.start 'rails' do
    minimum_coverage 100
    add_filter 'app/mailers/application_mailer'
    add_filter 'app/controllers/application_controller'
    add_filter 'app/jobs/application_job'
  end
end

RSpec.configure do |config|
  config.shared_context_metadata_behavior = :apply_to_host_groups
  config.default_formatter = 'doc' if config.files_to_run.one?
  config.run_all_when_everything_filtered = true
  config.filter_run_when_matching :focus
  config.disable_monkey_patching!
  Kernel.srand config.seed
  config.order = :random

  config.expect_with :rspec do |expectations|
    expectations.syntax = :expect
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.syntax = :expect
    mocks.verify_partial_doubles = true
  end
end
