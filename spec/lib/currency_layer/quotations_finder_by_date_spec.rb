require 'rails_helper'

RSpec.describe CurrencyLayer::QuotationsFinderByDate do
  describe 'constants' do
    it { expect(subject::URL).to eq 'http://apilayer.net/api/historical' }
    it { expect(subject::CURRENCIES).to eq 'BRL,EUR,ARS' }
  end

  describe '.call' do
    context 'with date argument' do
      let(:expected_response) do
        {
          'date'   => '2016-11-20',
          'source' => 'USD',
          'quotes' => {
            'USDBRL' => 3.392204,
            'USDEUR' => 0.944804,
            'USDARS' => 15.46996
          }
        }
      end

      let(:date)      { '2016-11-20' }
      let!(:response) { subject.call(date) }

      it 'returns expected date', :vcr do
        expect(response['date']).to eq expected_response['date']
      end

      it 'returns expected source', :vcr do
        expect(response['source']).to eq expected_response['source']
      end

      it 'returns expected BRL value', :vcr do
        expect(response['quotes']['USDBRL']).to eq expected_response['quotes']['USDBRL']
      end

      it 'returns expected EUR value', :vcr do
        expect(response['quotes']['USDEUR']).to eq expected_response['quotes']['USDEUR']
      end

      it 'returns expected ARS value', :vcr do
        expect(response['quotes']['USDARS']).to eq expected_response['quotes']['USDARS']
      end
    end

    context 'without date argument' do
      it 'raises argument error' do
        expect { subject.call }.to raise_error(ArgumentError)
      end
    end
  end
end
