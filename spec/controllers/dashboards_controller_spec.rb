require 'rails_helper'

RSpec.describe DashboardsController, type: :controller do

  describe 'GET #index' do
    let(:quotations) do
      [{"success"=>true,
        "terms"=>"https://currencylayer.com/terms",
        "privacy"=>"https://currencylayer.com/privacy",
        "historical"=>true,
        "date"=>"2016-11-23",
        "timestamp"=>1479945599,
        "source"=>"USD",
        "quotes"=>{"USDBRL"=>3.4321, "USDEUR"=>0.948302, "USDARS"=>15.489581}}]
    end

    before do
      allow(CurrencyQuotationsFinder).to receive(:call).and_return(quotations)
    end

    it 'calls CurrencyQuotationsFinder service' do
      expect(CurrencyQuotationsFinder).to receive(:call).and_return(quotations)

      get :index
    end

    it 'responds successfully with 200' do
      get :index

      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it 'returns index template' do
      get :index

      expect(response).to render_template(:index)
    end
    
    it 'assigns currency_values' do
      get :index

      expect(assigns(:currency_quotations)).to eq quotations
    end
  end
end
