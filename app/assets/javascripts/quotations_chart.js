'use strict';

$(document).ready(function() {
  fetchDashboard('Real', 'USDBRL');

  $('#target-brl-button').click(function(e){
    e.preventDefault();

    fetchDashboard('Real', 'USDBRL');
  });

  $('#target-eur-button').click(function(e){
    e.preventDefault();

    fetchDashboard('Euro', 'USDEUR');
  });

  $('#target-ars-button').click(function(e){
    e.preventDefault();

    fetchDashboard('Peso Argentino', 'USDARS');
  });

  function chartOptions() {
    var options = {
      chart: {
        renderTo: 'quotations-chart',
        type: 'area'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: []
      },
      yAxis: {
        title: {
          text: 'Valor'
        }
      },
      series: []
    }

    return options;
  }

  function plotChart(name, dates, quotations) {
    var charOptions = chartOptions();
    
    charOptions.xAxis.categories = dates;
    charOptions.yAxis.min = Math.min.apply(Math, quotations);
    charOptions.yAxis.max = Math.max.apply(Math, quotations);
    charOptions.title.text = 'Dólar x ' + name;
    charOptions.series = [ { name: name, data: quotations } ];

    new Highcharts.Chart(charOptions);
  }

  function fetchDashboard(currencyName, currencyFormat) {
    $.getJSON('dashboards.json', function(data) {

      var dates      = [];
      var quotations = [];

      for (var i = 0; i < data.length; i++) {
        var splittedDate  = data[i].date.split('-')
        var formattedDate = splittedDate[2] + '/' + splittedDate[1]

        dates.push(formattedDate);
        quotations.push(data[i].quotes[currencyFormat]);
      }

      plotChart(currencyName, dates, quotations);
    });
  }

});
