class DashboardsController < ApplicationController
  def index
    expires_in 1.hour

    @currency_quotations = CurrencyQuotationsFinder.call

    respond_to do |format|
      format.html
      format.json { render json: @currency_quotations }
    end
  end
end