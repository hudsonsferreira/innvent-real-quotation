class CurrencyQuotationsFinder
  include CurrencyLayer::QuotationsFinderByDate
  attr_reader :response_array

  def self.call
    new.call
  end

  def initialize
    @response_array = []
  end

  def call
    last_seven_days.each do |day|
      quotations = CurrencyLayer::QuotationsFinderByDate.call(day)
      response_array << quotations
    end

    response_array
  end

  private

  def last_seven_days
    (Date.current - 6)..Date.current
  end
end
